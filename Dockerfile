FROM python:3.10
RUN apt update
COPY requirements.txt requirements.txt
RUN pip install --no-cache-dir -r requirements.txt
RUN apt install fonts-nanum -y
RUN fc-cache -fv
RUN cp /usr/share/fonts/truetype/nanum/Nanum* /usr/local/lib/python3.10/site-packages/matplotlib/mpl-data/fonts/ttf/
RUN apt-get clean autoclean && apt-get autoremove --yes && rm -rf /var/lib/{apt,dpkg,cache,log}/
